// "document" refers to the whole webpage
// "querySelector" is used to select specific object(HTML element) from our document(webpage)
const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");

const spanFullName = document.querySelector("#span-full-name");

// document.getElementByID()
// document.getElementsByClassName()
// const txtFirstName = document.getElementByID("#txt-first-name");
// const spanFullName = document.getElementByID("#span-full-name");

/*Whenever a user interacts with a web page, this action is considered as an event
"addEventListenenr" takes two arguments
"keyup" - string identifying an event
function that the listener will execute once the specified event is triggered
txtFirstName.addEventListener('keyup', (event) =>
{
	// "innerHTML" property sets or returns the HTML content
	spanFullName.innerHTML = txtFirstName.value;

	// "event.target" contains the element where the event happened
	console.log(event.target);
	console.log(event.target.value);
})*/

/*S47 ACTIVITY START*/

// creating the function for making full name
function makeFullName()
{
	let firstName = txtFirstName.value;
	let lastName = txtLastName.value;

	spanFullName.innerHTML = `${firstName} ${lastName}`;
}

txtFirstName.addEventListener("keyup", makeFullName);
txtLastName.addEventListener("keyup", makeFullName);